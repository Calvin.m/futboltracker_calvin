package com.rave.futboltracker.view

import androidx.appcompat.app.AppCompatActivity
import com.rave.futboltracker.R
import dagger.hilt.android.AndroidEntryPoint

/**
 *
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)
