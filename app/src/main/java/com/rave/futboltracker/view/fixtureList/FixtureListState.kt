package com.rave.futboltracker.view.fixtureList

import com.rave.futboltracker.model.entity.Fixture

/**
 * Data class to hold the state properties for [FixtureListState].
 *
 * @property isLoading true if fetching fixtures, else false
 * @property fixures all available fixtures.
 * @constructor Create isntance of [FixtureListState]
 */
data class FixtureListState(
    val isLoading: Boolean = false,
    val fixtures: List<Fixture> = emptyList()
)
