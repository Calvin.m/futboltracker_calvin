package com.rave.futboltracker.view

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.futboltracker.databinding.ItemFixtureBinding
import com.rave.futboltracker.model.entity.Fixture
import com.rave.futboltracker.model.entity.MatchState

/**
 * Fixture adapter.
 *
 * @constructor Create empty Fixture adapter
 */
class FixtureAdapter : RecyclerView.Adapter<FixtureAdapter.FixturesViewHolder>() {

    private val fixtures: MutableList<Fixture> = mutableListOf()

    // the 3 methods you need for any adapter class

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FixturesViewHolder {
        return FixturesViewHolder.invoke(parent)
    }

    override fun onBindViewHolder(
        holder: FixturesViewHolder,
        position: Int
    ) {
        val fixture = fixtures[position]
        holder.bindFixture(fixture)
    }

    override fun getItemCount(): Int = fixtures.size

    fun loadFixtures(newFixtures: List<Fixture>) {
        val startPosition = this.fixtures.size
        this.fixtures.addAll(newFixtures)
        notifyItemRangeInserted(startPosition, newFixtures.size)
    }

    // View Holder sub-class
    class FixturesViewHolder(private val binding: ItemFixtureBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindFixture(fixture: Fixture) = with(binding) {
            tvHomeTeamName.text = fixture.homeTeamName
            tvAwayTeamName.text = fixture.awayTeamName
            tvLeagueName.text = fixture.leagueName

            tvVenueNameAndDateTime.text = fixture.run {"$venueName | $date"}

            val isPostponed = fixture.state == MatchState.POSTPONED
            if (isPostponed) {
                tvPostponed.visibility = View.VISIBLE
                tvVenueNameAndDateTime.setTextColor(Color.RED)
            } else {
                tvPostponed.visibility = View.GONE
                tvVenueNameAndDateTime.setTextColor(Color.GRAY)
            }
        }

        companion object {
            operator fun invoke(parent: ViewGroup): FixturesViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = ItemFixtureBinding.inflate(inflater, parent, false)
                return FixturesViewHolder(itemBinding)
            }
        }
    }
}
