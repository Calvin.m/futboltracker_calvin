package com.rave.futboltracker.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.futboltracker.databinding.FragmentFixtureListBinding
import com.rave.futboltracker.viewmodel.FixtureListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class FixtureListFragment : Fragment() {

    private val fixtureListViewModel by viewModels<FixtureListViewModel>()
    private var _binding: FragmentFixtureListBinding? = null
    private val fixtureAdapter by lazy { FixtureAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentFixtureListBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvFixtures.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@FixtureListFragment.fixtureAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fixtureListViewModel.state.observe(viewLifecycleOwner) { state ->
            fixtureAdapter.loadFixtures(state.fixtures)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
