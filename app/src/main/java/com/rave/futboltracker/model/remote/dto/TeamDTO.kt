package com.rave.futboltracker.model.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TeamDTO(
    @SerialName("abbr")
    val abbr: String,
    @SerialName("alias")
    val alias: String,
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    val name: String,
    @SerialName("shortName")
    val shortName: String
)
