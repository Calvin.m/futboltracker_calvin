package com.rave.futboltracker.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rave.futboltracker.model.remote.dto.FixtureResponseItemDTO
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET

/**
 * Service class to fetch Futbol related data.
 *
 * @constructor Create instance of [FutbolService]
 */
@OptIn(ExperimentalSerializationApi::class)
interface FutbolService {
    @GET(PATH_FIXTURES)
    suspend fun getFixtures(): List<FixtureResponseItemDTO>

    companion object {
        private const val BASE_URL = "https://storage.googleapis.com/"
        private const val PATH_FIXTURES = "cdn-og-test-api/test-task/fixtures.json"

        /**
         * Invoke.
         *
         * @return
         */
        // What is this doing?
        // transforms FutbolService into a callable function now
        operator fun invoke(): FutbolService {
            val json = Json {
                explicitNulls = false
                coerceInputValues = true
                ignoreUnknownKeys = true
            }
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .build()
                .create()
        }
    }
}
