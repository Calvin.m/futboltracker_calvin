package com.rave.futboltracker.model

import android.util.Log
import com.rave.futboltracker.model.entity.Fixture
import com.rave.futboltracker.model.entity.MatchState
import com.rave.futboltracker.model.remote.FutbolService
import com.rave.futboltracker.model.remote.dto.FixtureResponseItemDTO
import javax.inject.Inject

/**
 * Repository class to fetch fixtures.
 *
 * @property service used to fetch fixtures from server.
 * @constructor Create instance of [FutbolRepo]
 */
class FutbolRepo @Inject constructor(private val service: FutbolService) {

    /**
     * Get all fixtures.
     *
     * @return [Result] which contains [List] of [Fixture]
     */
    suspend fun getFixtures(): Result<List<Fixture>> {
        val fixturesDTO = service.getFixtures()
        val fixtures = fixturesDTO.map { it.mapToFixture() }
        return Result.success(fixtures)
    }

    private fun FixtureResponseItemDTO.mapToFixture(): Fixture {
        return Fixture(
            leagueName = this.competitionStage.competition.name,
            venueName = venue.name,
            date = date,
            homeTeamName = homeTeam.name,
            awayTeamName = awayTeam.name,
            state = when (state) {
                "postponed" -> MatchState.POSTPONED
                "prematch" -> MatchState.PRE_MATCH
//                else -> error("Unknown state: $state")
                else -> MatchState.UNKNOWN
            }
        )
    }
}
