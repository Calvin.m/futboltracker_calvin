package com.rave.futboltracker.model.di

import com.rave.futboltracker.model.remote.FutbolService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module // A Dagger annotation, use it to group dependencies we want to make available for injection
@InstallIn(SingletonComponent::class) // Determines the scope in which to install these dependencies
object NetworkModule {

    @Provides // signals that this is to be included in available dependencies
    @Singleton // Mainly for java, this helps make it a true singleton
    fun providesFutbolService(): FutbolService = FutbolService()
//    fun providesFutbolService(): FutbolService = FutbolService()
}
