package com.rave.futboltracker.model.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FixtureResponseItemDTO(
    @SerialName("awayTeam")
    val awayTeam: TeamDTO,
    @SerialName("competitionStage")
    val competitionStage: CompetitionStageDTO,
    @SerialName("date")
    val date: String,
    @SerialName("homeTeam")
    val homeTeam: TeamDTO,
    @SerialName("id")
    val id: Int,
    @SerialName("state")
//    val state: String = "",
    val state: String?,
    @SerialName("type")
    val type: String,
    @SerialName("venue")
    val venue: VenueDTO
)
