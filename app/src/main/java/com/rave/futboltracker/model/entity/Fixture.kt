package com.rave.futboltracker.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class Fixture(
    val leagueName: String,
    val venueName: String,
    val date: String, // Should be Date object
    val homeTeamName: String,
    val awayTeamName: String,
    val state: MatchState
)

/**
 * Match state.
 *
 * @constructor Create empty Match state
 */
enum class MatchState {
    PRE_MATCH, POSTPONED, UNKNOWN
}
