package com.rave.futboltracker.model.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CompetitionStageDTO(
    @SerialName("competition")
    val competition: CompetitionDTO,
    @SerialName("leg")
    val leg: String?,
//    val leg: String = "",
    @SerialName("stage")
    val stage: String?
//    val stage: String = ""
)
