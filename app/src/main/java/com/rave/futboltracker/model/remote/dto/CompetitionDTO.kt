package com.rave.futboltracker.model.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CompetitionDTO(
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    val name: String
)
