package com.rave.futboltracker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Futbol tracker application.
 *
 * @constructor Create empty Futbol tracker application
 */
@HiltAndroidApp
class FutbolTrackerApplication : Application()
