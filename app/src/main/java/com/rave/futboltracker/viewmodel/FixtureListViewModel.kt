package com.rave.futboltracker.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.futboltracker.model.FutbolRepo
import com.rave.futboltracker.model.entity.Fixture
import com.rave.futboltracker.view.fixtureList.FixtureListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Fixture list view model.
 *
 * @property repo
 * @constructor Create empty Fixture list view model
 */
@HiltViewModel
class FixtureListViewModel @Inject constructor( //Inject here allows view model to also inject this
    private val repo: FutbolRepo
) : ViewModel() {

    private val _stateFlow = MutableStateFlow(FixtureListState())
    val stateFlow: StateFlow<FixtureListState> get() = _stateFlow

    private val _state = MutableLiveData(FixtureListState())
    val state: LiveData<FixtureListState> get() = _state

    init {
        fetchFixtures()
    }

    private fun fetchFixtures() {
        _state.isLoading(true)
        viewModelScope.launch {
            repo.getFixtures()
                .onSuccess { fixtures -> _state.isSuccess(fixtures) }
                .onFailure { _state.isLoading(false) }
        }
    }

    private fun MutableLiveData<FixtureListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<FixtureListState>.isSuccess(fixtures: List<Fixture>) {
        value = value?.copy(isLoading = false, fixtures = fixtures)
    }
}
